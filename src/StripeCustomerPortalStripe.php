<?php

namespace Drupal\stripe_customer_portal;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;
use Stripe\BillingPortal\Session;

/**
 * A service wrapper for Stripe PHP library.
 */
class StripeCustomerPortalStripe {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a StripeWebformPaymentService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('stripe_customer_portal');
    $this->messenger = $messenger;
  }

  /**
   * Gets the Stripe API credentials.
   *
   * @return array|null
   *   Array of API publishable key, API secret, and webhook secret.
   */
  private function getCredentials(): ?array {
    $config = $this->configFactory->get('stripe.settings');
    return $config->get('apikey.' . $config->get('environment'));
  }

  /**
   * Get the Stripe API publishable key.
   *
   * @return string
   *   The Stripe API publishable key.
   */
  public function getPublishableKey(): string {
    $credentials = $this->getCredentials();

    return $credentials['public'];
  }

  /**
   * Get the Stripe API secret.
   *
   * @return string
   *   The Stripe API secret.
   */
  private function getApiSecret(): string {
    $credentials = $this->getCredentials();

    return $credentials['secret'];
  }

  /**
   * Get the payment intent object.
   *
   * @param string $customerId
   *   The Stripe customer ID.
   * @param string $returnUrl
   *   The return URL.
   *
   * @return \Stripe\BillingPortal\Session|null
   *   Payment intent object if any.
   */
  public function getCustomerSession(string $customerId, string $returnUrl) {

    try {
      Stripe::setApiKey($this->getApiSecret());
      return Session::create([
        'customer' => $customerId,
        'return_url' => $returnUrl,
      ]);

    }
    catch (ApiErrorException $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError(t('Error fetching Stripe product price: @code - @description', [
        '@code' => $e->getStripeCode(),
        '@description' => $e->getError()->message,
      ]));
      return NULL;
    }
  }

}
