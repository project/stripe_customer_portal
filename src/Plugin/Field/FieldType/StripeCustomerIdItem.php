<?php

namespace Drupal\stripe_customer_portal\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'stripe_customer_portal_stripe_customer_id' field type.
 *
 * @FieldType(
 *   id = "stripe_customer_id",
 *   label = @Translation("Stripe Customer ID"),
 *   category = @Translation("General"),
 *   default_widget = "string_textfield",
 *   default_formatter = "stripe_customer_portal_link",
 *   description = @Translation("A field to store Stripe Customer ID.")
 * )
 */
class StripeCustomerIdItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Stripe customer ID'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
        'Regex' => [
          'pattern' => '/^cus_[A-Za-z0-9]{14,}$/',
          'message' => $this->t('The Stripe Customer ID must start with "cus_" followed by 14 or more alphanumeric characters.'),
        ],
        'Length' => [
          'max' => 255,
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'value' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'The Stripe Customer ID.',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

}
