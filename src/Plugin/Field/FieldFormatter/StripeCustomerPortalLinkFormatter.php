<?php

namespace Drupal\stripe_customer_portal\Plugin\Field\FieldFormatter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\stripe_customer_portal\StripeCustomerPortalStripe;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Stripe Customer Portal Link' formatter.
 *
 * @FieldFormatter(
 *   id = "stripe_customer_portal_link",
 *   label = @Translation("Stripe Customer Portal Link"),
 *   field_types = {
 *     "stripe_customer_id"
 *   }
 * )
 */
class StripeCustomerPortalLinkFormatter extends FormatterBase {

  /**
   * The Stripe Customer Portal Stripe service.
   *
   * @var \Drupal\stripe_customer_portal\StripeCustomerPortalStripe
   */
  protected $stripe;

  /**
   * Constructs a StripeCustomerPortalLinkFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\stripe_customer_portal\StripeCustomerPortalStripe $stripe
   *   The Stripe Customer Portal Stripe service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, StripeCustomerPortalStripe $stripe) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->stripe = $stripe;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('stripe_customer_portal.stripe')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link_title' => 'Customer Portal',
      'return_url' => '/',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['link_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Title'),
      '#description' => $this->t('Set the title of the Stripe Customer Portal link'),
      '#default_value' => $this->getSetting('link_title'),
      '#required' => TRUE,
    ];

    $element['return_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Return URL'),
      '#description' => $this->t('The URL that Stripe Customer Portal will use to return users to the website. You may use full link starting with (https://) or local links starting with (/)'),
      '#default_value' => $this->getSetting('return_url'),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Stripe customer portal link title: @link_title', ['@link_title' => $this->getSetting('link_title')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Get the link title setting from the formatter.
    $linkTitle = $this->getSetting('link_title');
    // Get the return URL setting from the formatter.
    $returnUrl = $this->getSetting('return_url');

    if (!str_starts_with($returnUrl, 'http')) {
      $baseUrl = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
      $returnUrl = $baseUrl . ltrim($returnUrl, '/');
    }

    foreach ($items as $delta => $item) {
      // Generate the link to the Stripe Customer Portal.
      $stripeCustomerId = $item->value;
      $portalUrl = $this->getCustomerPortalUrl($stripeCustomerId, $returnUrl);

      if (!$portalUrl) {
        $this->messenger()->addError($this->t("Couldn't get the portal URL, contact your system administrator."));
        return NULL;
      }

      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '<a href="{{ url }}" class="stripe-customer-portal-link" target="_blank">{{ title }}</a>',
        '#context' => [
          'url' => $portalUrl,
          'title' => $this->t($linkTitle),
        ],
        '#cache' => [
          'contexts' => ['url'],
        // Set the cache max age to 1 hour (3600 seconds).
          'max-age' => 3600,
        ],
      ];
    }

    return $elements;
  }

  /**
   * Get the Stripe Customer Portal URL for a user.
   *
   * @param string $stripeCustomerId
   *   The Stripe customer ID,.
   * @param string $return_url
   *   The return URL.
   *
   * @return string|null
   *   Stripe customer portal URL if exists.
   */
  protected function getCustomerPortalUrl(string $stripeCustomerId, string $return_url): ?string {
    $session = $this->stripe
      ->getCustomerSession($stripeCustomerId, $return_url) ?? NULL;
    return $session?->url ?? NULL;
  }

}
